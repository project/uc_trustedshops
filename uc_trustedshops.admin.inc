<?php

/**
 * @file
 * Forms to configure TrustedShops for Drupal.
 */

/**
 *
 */
function uc_trustedshops_settings() {
  $form['uc_trustedshops_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate TrustedShops.'),
    '#default_value' => uc_trustedshops_var('active'),
  );
  $form['uc_trustedshops_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Your TrustedShops ID'),
    '#size' => 50,
    '#maxlength' => 50,
    
    '#default_value' => uc_trustedshops_var('id'),
  );
  $form['uc_trustedshops_seal_code'] = array(
    '#type' => 'textarea',
    '#title' => t('Your seal-code'),
    '#size' => 10,
    '#maxlength' => 5000,
    '#default_value' => uc_trustedshops_var('seal_code'),
    '#description' => t('Please generate your seal-code !url and copy the result into this textbox.
      You can ignore the instructions to copy graphic files onto you web server, this module takes care
      of this automatically.', array('!url' => l('here', 'http://www.trustedshops.de/shopbetreiber/siegelbox/'))),
  );
  $form['#validate'][] = 'uc_trustedshops_settings_validate';
  return system_settings_form($form);
}

function uc_trustedshops_settings_validate($form, &$form_state) {
  $code = $form_state['values']['uc_trustedshops_seal_code'];
  $path = '/'. drupal_get_path('module', 'uc_trustedshops') .'/image/';
  $code = str_replace('url(images/', 'url('. $path, $code);
  $code = str_replace('src="images/', 'src="'. $path, $code);
  $form_state['values']['uc_trustedshops_seal_code'] = $code;
}
