
-- SUMMARY --

Confidence in eCommerce is a big issue and seals or certificates from respected
institutions help to gain visitor's trust in online shops much better than without
them and it gets the visitors to spend more money.

One of those trusted body's in Germany is TrustedShop. They have analyzed and approved
several thousand online shops so far and granted their highly regarded certificates.

Online shop customers do have the choice after completing an order in a TrustedShop-certified
onlie-shop to sign up for an insurance for that order with full money back guarantee.

This module (uc_trustedshops) integrates full functionality of the TruestedShop range
of services including the display of the certificate in a block as well as integration into
the orderprocess from Ubercart to allow customers to select the insurance.

More details about TrustedShops can be found on their website (http://www.trustedshops.com).

-- REQUIREMENTS --

* Drupal 6 and Ubercart module 2.0.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure eKomi in Administer >> Store Administration >> TrustedShops

* Make the TrustedShops block visible in a region of your site in Structure >> blocks

-- CUSTOMIZATION --

None

-- TROUBLESHOOTING --

Not known yet

-- FAQ --

Yet to come

-- CONTACT --

Current maintainer:
* J�rgen Haas (jurgenhaas) - http://drupal.org/user/168924

This project has been sponsored by:
* PARAGON Executive Services GmbH
  Providing IT services as individual as the requirements. Find out more
  from http://www.paragon-es.de
