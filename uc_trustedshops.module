<?php

/**
 * @file
 * The main file for the uc_trustedshops module.
 *
 * This module displays the individual seal for the shop as well as implements the sign-up
 * functionality for an insurance for orders in the Ubercart online shop.
 */

/**
 * Implementation of hook_help().
 */
function uc_trustedshops_help($path, $arg) {
  switch ($path) {
    case 'admin/store/trustedshops':
      return '<p>'. t('Here you can set up parameters for TrustedShops.') .'</p>';
    case 'admin/help#uc_trustedshops':
      return filter_filter('process', 2, NULL, file_get_contents(dirname(__FILE__) ."/README.txt"));
  }
}

/**
 * Implementation of hook_menu().
 */
function uc_trustedshops_menu() {
  $items = array();
  $access_config = array('administer site configuration');
  $items['admin/store/trustedshops'] = array(
    'title' => 'TrustedShops',
    'description' => 'Configure TrustedShops',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_trustedshops_settings'),
    'access arguments' => $access_config,
    'type' => MENU_LOCAL_TASK,
    'file' => 'uc_trustedshops.admin.inc',
  );
  return $items;
}

/**
 * Internal default variables for template_var().
 */
function uc_trustedshops_variables() {
  return array(
    'uc_trustedshops_active' => FALSE,
    'uc_trustedshops_id' => '',
    'uc_trustedshops_seal_code' => '',
  );
}

/**
 * Internal implementation of variable_get().
 */
function uc_trustedshops_var($name, $default = NULL) {
  static $defaults = NULL;
  if (!isset($defaults)) {
    $defaults = uc_trustedshops_variables();
  }
  $name = 'uc_trustedshops_'. $name;
  if (!isset($defaults[$name])) {
    drupal_set_message(t('Default variable for %variable not found.', array('%variable' => $name)), 'error');
    watchdog('uc_trustedshops', 'Default variable for %variable not found.', array('%variable' => $name), WATCHDOG_WARNING);
  }
  return variable_get($name, isset($default) || !isset($defaults[$name]) ? $default : $defaults[$name]);
}

/**
 * Implementation of hook_block().
 */
function uc_trustedshops_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0]['info'] = t('TrustedShops Seal');
      $blocks[0]['cache'] = BLOCK_NO_CACHE;
      return $blocks;
    case 'configure':
      return;
    case 'save':
      return;
    case 'view':
      $seal = uc_trustedshops_var('seal_code');
      if (uc_trustedshops_var('active') AND uc_trustedshops_var('id') AND !empty($seal)) {
        $blocks['subject'] = '';
        $blocks['content'] = $seal;
      }
      else {
        $url = url("admin/store/trustedshops/settings", array('absolute' => TRUE));
        $blocks['subject'] = 'TrustedShops Seal';
        $blocks['content'] = t('Please configure your TrustedShops account at !url', array('!url' => l($url, $url)));
      }
      return $blocks;
  }
}

/**
 * Implementation of hook_order().
 */
function uc_trustedshops_order($op, &$arg1, $arg2) {
  switch ($op) {
    case 'submit':
      $logo = '/'. drupal_get_path('module', 'uc_trustedshops') .'/image/trustedshops_m.gif';
      $id = uc_trustedshops_var('id');
      $email = $arg1->primary_email;
      $value = $arg1->order_total;
      $currency = "EUR";
      $payment = '10'; //PayPal
      switch ($arg1->payment_method) {
        case 'cod':
          $payment = '5';
          break;
        case 'other':
          $payment = '7';
          break;
      }
      $cid = $arg1->uid;
      $oid = $arg1->order_id;
      $msg = <<<EOT
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr>
            <td width="90">
              <form name="formSiegel" method="post"
                    action="https://www.trustedshops.com/shop/certificate.php" target="_blank">
                <input type="image" border="0" src="$logo" title="Trusted Shops G&uuml;tesiegel - Bitte hier G&uuml;ltigkeit pr&uuml;fen!">
                <input name="shop_id" type="hidden" value="$id">
              </form>
            </td>
            <td align="justify">
              <form id="formTShops" name="formTShops" method="post" action="https://www.trustedshops.com/shop/protection.php" target="_blank">
                <input type="hidden" name="_charset_">
                <input name="shop_id" type="hidden" value="$id">
                <input name="email" type="hidden" value="$email">
                <input name="amount" type="hidden" value="$value">
                <input name="curr" type="hidden" value="$currency">
                <input name="payment" type="hidden" value="$payment">
                <input name="KDNR" type="hidden" value="$cid">
                <input name="ORDERNR" type="hidden" value="$oid">
                <font face="Verdana, Arial, Helvetica, Geneva, sans-serif" 
                  size="1" color="#000000">
                  Als zus&auml;tzlichen Service bieten wir Ihnen den Trusted Shops 
                  K&auml;uferschutz an. Wir &uuml;bernehmen alle Kosten dieser Garantie, 
                  Sie m&uuml;ssen sich lediglich anmelden.<br><br>
                  <input type="submit" id="btnProtect" name="btnProtect" value="Anmeldung zum Trusted Shops K&auml;uferschutz">
                </font>
              </form>
            </td>
          </tr>
        </table>
EOT;
      return array(array('pass' => TRUE, 'message' => t($msg)));
  }
}
